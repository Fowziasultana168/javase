package pattern_recognition;

import java.util.Scanner;

public class StarPattern {

    public static void main(String[] args) {
        int i, j, k;


        for (i = 1; i <= 2; i++) {
            for (k = 1; k <= i; k++) {
                System.out.printf("*");
            }
            System.out.println();
        }
        for (i = 1; i <= 4; i++) {
            System.out.printf("*");
        }

        System.out.println();

        for (i = 2; i >= 1; i--) {
            for (k = i; k >= 1; k--) {
                System.out.printf("*");
            }
            System.out.println();
        }


    }
}
