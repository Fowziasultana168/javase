package count_odd_even;

import java.util.ArrayList;
import java.util.Scanner;

public class OddEvenCounter {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.println("Enter the starting number : ");
        int firstNumber = reader.nextInt();

        System.out.println("Enter the ending number : ");
        int lastNumber = reader.nextInt();

        int i, countTotal = 0, countEven = 0;
        ArrayList<Integer> input = new ArrayList<>();

        for (i = firstNumber; i <= lastNumber; i++) {
            countTotal++;
        }

        if (firstNumber % 2 == 0) {
            for (i = firstNumber; i <= lastNumber; i += 2) {
                input.add(i);
                countEven++;
            }

            int countOdd = countTotal - countEven;

            System.out.println("Total even number count: " + countEven);
            System.out.println("Total odd number count: " + countOdd);
            System.out.println("Total even numbers are: " + input);
        } else {
            int countOdd = 0;
            for (i = firstNumber; i <= lastNumber; i += 2) {
                input.add(i + 1);
                countOdd++;
            }

            countEven = countTotal - countOdd;


            System.out.println("Total even number count: " + countEven);
            System.out.println("Total odd number count: " + countOdd);
            System.out.println("Total even numbers are: " + input.subList(0, input.size() - 1));


        }


    }
}
