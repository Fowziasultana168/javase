package calculator;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the first number : ");
        double firstNumber = scanner.nextDouble();

        System.out.println("Enter the operator : ");
        char operator = scanner.next().charAt(0);

        System.out.println("Enter the second number : ");
        double secondNumber = scanner.nextDouble();

        double result = 0;


        switch (operator) {
            case '+':
                result = addNumber(firstNumber, secondNumber);
                printResult(firstNumber, secondNumber, result, operator);
                break;
            case '-':
                result = substractNumber(firstNumber, secondNumber);
                printResult(firstNumber, secondNumber, result, operator);
                break;
            case '*':
                result = multiplyNumber(firstNumber, secondNumber);
                printResult(firstNumber, secondNumber, result, operator);
                break;
            case '/':
                result = divideNumber(firstNumber, secondNumber);
                printResult(firstNumber, secondNumber, result, operator);
                break;
            default:
                System.out.println("operator is not correct ");
                break;

        }

    }

    static double addNumber(double x, double y) {
        return x + y;
    }

    ;

    static double substractNumber(double x, double y) {
        return x - y;
    }

    ;

    static double multiplyNumber(double x, double y) {
        return x * y;
    }

    ;

    static double divideNumber(double x, double y) {
        return x / y;
    }

    ;

    static void printResult(double x, double y, double result, char operator) {
        System.out.printf("%.3f %c %.3f = %.3f", x, operator, y, result);
    }

    ;


}
