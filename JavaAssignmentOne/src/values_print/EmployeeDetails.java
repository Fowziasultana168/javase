package values_print;

public class EmployeeDetails {
    String name, gender;
    int id;

    EmployeeDetails() {
        System.out.println("Its an default constructor");
    }

    EmployeeDetails(String n, String g, int i) {
        name = n;
        gender = g;
        id = i;

    }


    void displayFunction() {

        System.out.println("The name is: " + name);
        System.out.println("The gender is: " + gender);
        System.out.println("The id is: " + id);
    }

    ;


}
