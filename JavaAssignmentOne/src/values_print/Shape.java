package values_print;

public abstract class Shape {
    double height, width;

    Shape(double x, double y) {
        this.height = x;
        this.width = y;
    }

    abstract void area();
}
