package values_print;

public class Rectangle extends Shape {
    Rectangle(double height, double width) {
        super(height, width);
    }

    void area() {
        double result = height * width;
        System.out.println("Area of rectangle is: " + result);
    }


}
