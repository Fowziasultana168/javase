package values_print;

public class BlockStaticNonstatic {
    {
        System.out.println("First Non-Static Block"); // first non-static block
    }

    {
        System.out.println("Second Non-Static Block"); // second non-static block
    }

    static String studentName;
    static int roll;

    static {
        studentName = "Nusfat Tasnim";
        roll = 3;
    }

    static void display() {
        System.out.println("Your students Name from static block is: " + studentName);
        System.out.println("Your students Id from static block is: " + roll);

    }

    public static void main(String[] args) {
        BlockStaticNonstatic blockStatic = new BlockStaticNonstatic();//non static block
        BlockStaticNonstatic.display();//Static block ex

        System.out.println("This wiil be implementation of Constructor with parameter");
        EmployeeDetails employeeDetails = new EmployeeDetails("mahjabin", "female", 17);
        employeeDetails.displayFunction();//constructor with parameter

        EmployeeDetails employeeDetails1 = new EmployeeDetails();// constructor without parameter

        System.out.println("This wiil be implementation of abstract class");

        Shape shape;
        shape = new Rectangle(10, 20);
        shape.area();

    }

}
